import React, {Component} from 'react';
import {AsyncStorage, Text, View, Alert} from 'react-native';
import firebase from 'react-native-firebase';
import NotificationPopup from 'react-native-push-notification-popup';

const APP_NAME = 'Push Notification Firebase';
const APP_ICON_LOCATION = require('./assets/ic_launcher.png');
export default class App extends Component {
    async componentDidMount() {
        this.checkPermission();
        this.createNotificationListeners(); //add this line
    }

    //Remove listeners allocated in createNotificationListeners()
    componentWillUnmount() {
        this.notificationListener();
        this.notificationOpenedListener();
    }

    //1
    async checkPermission() {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            this.getToken();
        } else {
            this.requestPermission();
        }
    }

    //3
    async getToken() {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                // user has a device token
                await AsyncStorage.setItem('fcmToken', fcmToken);
            }
        }
    }

    //2
    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
            // User has authorised
            this.getToken();
        } catch (error) {
            // User has rejected permissions
            console.log('permission rejected');
        }
    }

    async createNotificationListeners() {
        firebase.messaging().subscribeToTopic("Funny");

        /*
        * Triggered when a particular notification has been received in foreground
        * */
        this.notificationListener = firebase.notifications().onNotification((notification) => {
            const {title, body} = notification;

            this.popup.show({
                onPress: function () {
                    console.log('Pressed')
                },
                appIconSource: APP_ICON_LOCATION,
                appTitle: APP_NAME,
                timeText: 'Now',
                title: title,
                body: body,
                slideOutTime: 5000
            });

        });

        /*
        * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
        * */
        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            const {title, body} = notificationOpen.notification;
            this.popup.show({
                onPress: function () {
                    console.log('Pressed')
                },
                appIconSource: APP_ICON_LOCATION,
                appTitle: APP_NAME,
                timeText: 'Now',
                title: title,
                body: body,
                slideOutTime: 5000
            });
        });

        /*
        * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
        * */
        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            const {title, body} = notificationOpen.notification;
            this.popup.show({
                onPress: function () {
                    console.log('Pressed')
                },
                appIconSource: APP_ICON_LOCATION,
                appTitle: APP_NAME,
                timeText: 'Now',
                title: title,
                body: body,
                slideOutTime: 5000
            });
        }
        /*
        * Triggered for data only payload in foreground
        * */
        this.messageListener = firebase.messaging().onMessage((message) => {
            //process data message
            console.log(JSON.stringify(message));
        });
    }

    showAlert(title, body) {
        Alert.alert(
            title, body,
            [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            {cancelable: false},
        );
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <NotificationPopup ref={ref => this.popup = ref} />
                <Text>Welcome to React Native!</Text>
            </View>
        );
    }
}